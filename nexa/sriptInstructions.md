

# Nexa Script Instructions


[How to read these pages](/nexa/opcodesyntax.md)


## New Instructions

[EXEC](/nexa/op_exec.md)
[PLACE](/nexa/op_place.md)
[BIGNUM2BIN](/nexa/op_bignum2bin.md)
[BIN2BIGNUM](/nexa/op_bin2bignum.md)
[SETBMD](/nexa/op_setbmd.md)
[PUSHTXSTATE](/nexa/op_push_tx_state.md)
