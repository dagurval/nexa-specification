


# Nexa Transactions

[Change Summary](/nexa/transaction.md)

[Transaction Identifiers (Idem and Id)](/nexa/transactionIdentifier.md)
[Transaction Format](/protocol/blockchain/transaction.md)
[Script Template Output Type](/nexa/scriptTemplates.md)
[Signature Hash Type](/nexa/sighashtype.md)

[NexaScript Instructions](/nexa/scriptInstructions.md)
