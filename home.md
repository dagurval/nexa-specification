<div class="cwikmeta">  
{  
"title": "NEXA",
"related":["https://reference.cash"],
"summary": "Nexa cryptocurrency consensus and protocol specifications"
} </div>


# Nexa Blockchain Protocol and Consensus Specifications

### About

The Nexa consensus protocol is a modification of Bitcoin Cash, which is a modification of Bitcoin.  This documentation attempts to cover the full Nexa protocol, by leveraging previously written documentation for Bitcoin Cash.  Therefore you may occasionally see references to Bitcoin Cash that are no longer applicable.  Please notify us on [our issue tracker](https://gitlab.com/nexa/specification/-/issues) if you discover a problem.

For readers that are familiar with Bitcoin Cash, the "Nexa Blockchain Changes" section contains all Nexa-specific changes.

[Style Guide](/style-guide) -- [Contributors](/contributors) -- [Target Audience](/target-audience) -- [Project History](/project-history)

### Nexa Blockchain Changes

### Consensus Changes
[Blocks](/nexa/blockOverview.md)
[Transactions](/nexa/transactionOverview.md)
[Native Tokenization](/nexa/grouptokens.md)

### Protocol Changes
[Addresses](/nexa/address.md)
[Nexa Identity](/nexa/nexid.md)
[CAPD Message Pool (Counterparty and Protocol Discovery)](/nexa/capd.md)
[DPP (Delegated Payment Protocol)](/nexa/dpp.md)

### Basics
[Overview](/protocol/overview)
[Blockchain Basics](/protocol/blockchain)
[Protocol Hashing Algorithms](/protocol/blockchain/hash)
[Memory Pool](/protocol/blockchain/memory-pool)

### Transactions
[Transaction Format](/protocol/blockchain/transaction)
[Unlocking Script](/protocol/blockchain/transaction/unlocking-script)
[Locking Script](/protocol/blockchain/transaction/locking-script)
[Transaction Signing](/protocol/blockchain/transaction/transaction-signing)

### Blocks
[Bitcoin Blocks](/protocol/blockchain/block)
[Block Header](/protocol/blockchain/block/block-header)
[Merkle Tree](/protocol/blockchain/block/merkle-tree)
[Transaction Ordering](/protocol/blockchain/block/transaction-ordering)

### Script (Bitcoin transaction language)
[Script](/protocol/blockchain/script)
[Operation Codes (opcodes)](/protocol/blockchain/script#operation-codes-opcodes)

### Transaction validation
[Transaction Validation](/protocol/blockchain/transaction-validation)
[Block-Level Validation Rules](/protocol/blockchain/transaction-validation/block-level-validation-rules)
[Network-Level Validation Rules](/protocol/blockchain/transaction-validation/network-level-validation-rules)

### Proof of Work (PoW)
[Proof of Work](/protocol/blockchain/proof-of-work)
[Difficulty Adjustment Algorithm](/protocol/blockchain/proof-of-work/difficulty-adjustment-algorithm)
[Mining](/protocol/blockchain/proof-of-work/mining)
[Stratum Protocol](/mining/stratum-protocol)
[Mining Pools](/mining/mining-pools)

### Cryptography
[Bitcoin Keys (Public/Private)](/protocol/blockchain/cryptography/keys)
[Signatures (ECDSA/Schnorr)](/protocol/blockchain/cryptography/signatures)
[Multisignature (M-of-N multisig)](/protocol/blockchain/cryptography/multisignature)

### Network protocol
[Network Messages](/protocol/network/messages)
[Handshake](/protocol/network/node-handshake)

### Addresses
[Address Types](/protocol/blockchain/addresses)
[Cashaddr Encoding](/protocol/blockchain/encoding/cashaddr)

### Simple Payment Verification (SPV)
[SPV](/protocol/spv)
[Bloom Filters](/protocol/spv/bloom-filter)

### Miscellaneous
[Endian](/protocol/misc/endian)