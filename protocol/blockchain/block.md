<div class="cwikmeta">  
{  
"title": "Nexa Block",
"related": ["/protocol/blockchain/block.md","/protocol/blockchain/block/block-header.md"]
} </div>


# Block

A block is a collection of transactions that have been committed to the blockchain as a bundle.
The transactions do not necessarily have to be related to one another and, generally, are only associated by virtue of when they were submitted to the Bitcoin Cash network.

Every block contains one coinbase transaction, followed by zero or more additional transactions.
The coinbase transaction is created by the miner of the block and provides an opportunity for them to receive the block reward for successfully mining a block, along with any fees collected from the transactions in the block.
For more information, see [Mining](/protocol/mining).

## Block Headers

Blocks are identified by the double SHA-256 hash of their header.
Since the block header contains the root of a [Merkle Tree](/protocol/blockchain/merkle-tree) of the transactions in the block, this means the hash is ultimately dependent on both the block header and the full contents of all of its component transactions.

For more information see [Block Header](/protocol/blockchain/block/block-header).

## Coinbase Transaction

The coinbase transaction provides a mechanism for miners to:

 - Receive payment for mining.
 - Include a message, known as the "coinbase message" within the block.

The coinbase transaction MUST have no inputs and be the first transaction in the block.  The last output of the coinbase transaction MUST be an OP_RETURN (data carrier) script.

The satoshis that appear in the outputs of the coinbase transaction are collected from two places: the block reward and transactions fees.  Transaction fees are often required by the network for transactions to be relayed across the network.
Satoshis provided as inputs to a transaction, but not consumed by its outputs, are collected by the coinbase transaction as implicit inputs.

The last output is required to begin with OP_RETURN, followed by a push of the block height, and finally the coinbase message (which can be whatever data is desired).
The block height encoded as a minimally-encoded script number (see transaction scripts).

## Block Size

The maximum block size for blocks varies based on prior block sizes.
For more information see [Adaptive Block Size](/nexa/doc/adaptive-blocksize.md).
