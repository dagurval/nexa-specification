# Difficulty Adjustment Algorithm

In order to correct for changes in the network's total hashing power (i.e. as hardware improves or mining nodes are added to or removed from the network), the amount of work required to mine a block must change.
Nexa solves this by adjusting the [target](/protocol/blockchain/proof-of-work#target) according to an algorithm that looks at recent block timestamps, infers the hashing power that led to those timestamps, and attempts to change the [difficulty](/protocol/blockchain/proof-of-work#difficulty) of mining future blocks accordingly.
The calculation used is referred to as the Difficulty Adjustment Algorithm, or DAA, and is a control theory problem.

The algorithms used is [ASERT](#asert)

## ASERT

Absolutely Scheduled Exponentially Rising Targets (ASERT), more specifically [aserti3-2d](/protocol/forks/2020-11-15-asert), was implemented as a part of [HF-20201115](/protocol/forks/hf-20201115).
It uses an [exponential moving average](https://en.wikipedia.org/wiki/Moving_average#Exponential_moving_average) approach that should theoretically always target a correction toward the correct average block time.
ASERT bases it's calculations on the following components:

 1. The **anchor block**: The Nexa genesis block
 3. The current head block

Though this is not used directly in practice, the exponential form of the calculation of the target for the next block is:

```
exponent = (time_delta - ideal_block_time * (height_delta + 1)) / halflife
next_target = anchor_target * 2**(exponent)
```

where:

- `anchor_target` is the unsigned 256 bit integer equivalent of the `nBits` value in
  the header of the anchor block.
- `time_delta` is the difference, in signed integer seconds, between the
  timestamp in the header of the current block and the timestamp in the
  parent of the anchor block.
- `ideal_block_time` is a constant: 600 seconds, the targeted
  average time between blocks.
- `height_delta` is the difference in block height between the current
  block and the anchor block.
- `halflife` is a constant parameter sometimes referred to as
  'tau', with a value of 172800 (seconds) on mainnet.
- `next_target` is the integer value of the target computed for the block
  after the current block.

In order to avoid subtle platform-dependent floating point issues, however, ASERT is instead calculated using fixed-point integer arithmetic with a cubic polynomial approximation of the exponential.
See [ASERT:target computeration](/protocol/forks/2020-11-15-asert#target-computation) for the Python reference implementation and additional details on where new implementations of the algorithm must be cautious to ensure full compatibility.
